'use strict'

process.env.VUE_APP_BASE_API = "/hjw-server"

module.exports = {
  devServer: {
    proxy: {
      '/hjw-server': {
        target: 'http://127.0.0.1:8088',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/hjw-server': '/'
        }
      }
    }
  }
}