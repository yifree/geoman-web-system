import Base from '../Base'
import { Map, LayerGroup, TileLayer, GridLayer } from 'leaflet'
import L from 'leaflet'
import wmts from './wmts'

export default class LeafletMap extends Base {
  init(div, params, options) {
    options = options || {}
    this.map = new Map(div, options)
    this.layers = {}
  }


  initLayers(params) {
    this._initSubLayers(null, params)
  }

  _initSubLayers(parent, params) {
    for (const param of params) {
      var option = {}
      if (param.type === Base.LAYER_TYPE_GROUP) {
        var layer = new LayerGroup()
        var symbol = Symbol()
        param.__layer = symbol
        this.layers[symbol] = groupLayer;
        if (parent) {
          this.layers[parent].addLayer(layer)
        }
        _initSubLayers(groupLayer, children)
      } else {
        new LayerGroup(null, {})
      }
    }
  }

  createLayer(params) {
    var lyr = null;
    var opts = params.options || {}
    opts.minZoom = params.layer_view_range[0]
    opts.maxZoom = params.layer_view_range[1] || 20
    switch (params.layer_type) {
      case 'wms':
        lyr = L.tileLayer.wms(params.layer_url, opts)
        break
      case 'wmts':
        lyr = L.tileLayer.wmts(params.layer_url, params.options)
        break
      case 'xyz':
        lyr = L.tileLayer(params.layer_url, params.options)
        break
    }



    return lyr

  }
}