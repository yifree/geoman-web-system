import { createRouter, createWebHashHistory } from 'vue-router'
import Layout01 from '@/components/layout/layout01'
import Layout02 from '@/components/layout/layout02'

const routes = [{
  path: '/login',
  name: 'Login',
  hidden: true,
  component: () => import('@/components/pages/login'),
}, {
  path: '/register',
  hidden: true,
  name: 'Register',
  component: () => import('@/components/pages/register'),
},
{
  path: '/404',
  hidden: true,
  component: () => import('@/components/pages/404')
},
{
  path: '/',
  component: Layout01,
  redirect: '/home',
  children: [
    {
      path: '/home',
      name: 'Home',
      component: () => import('@/views/Home'),
    },
    {
      path: '/personal',
      name: "Personal",
      component: () => import('@/views/Home'),
    },
    {
      path: '/admin',
      name: 'Admin',
      component: () => import('../components/pages/admin/index.vue'),
      redirect: '/admin/user',
      children: [{
        path: 'user',
        title: '用户管理',
        name: 'User',
        component: () => import('../components/pages/admin/user/index.vue'),
      }, {
        path: 'role',
        name: 'Role',
        component: () => import('../components/pages/admin/role/index.vue'),
      }, {
        path: 'func',
        name: 'Func',
        component: () => import('../components/pages/admin/func/index.vue'),
      }, {
        path: 'dept',
        name: 'Group',
        component: () => import('../components/pages/admin/group/index.vue'),
      }, {
        path: 'dict',
        name: 'Dict',
        component: () => import('../components/pages/admin/dict/index.vue'),
      }, {
        path: 'district',
        name: 'District',
        component: () => import('../components/pages/admin/district/index.vue'),
      }, {
        path: 'sysinfo',
        name: 'Sysinfo',
        component: () => import('../components/pages/admin/sysinfo/index.vue'),
      }, {
        path: 'log',
        name: 'Log',
        component: () => import('../components/pages/admin/log/index.vue'),
      }, {
        path: 'config',
        name: 'Config',
        component: () => import('../components/pages/admin/sysinfo'),
      }]
    }
  ]
},


{
  path: '/about',
  name: 'About',
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
},
{ path: '/:pathMatch(.*)', redirect: '/404', hidden: true }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
