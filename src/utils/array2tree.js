export function array2tree(arr, childrenProp, idProp, pidProp, rootPidValue, sort) {
  var tree = arr.filter(r => r[pidProp] === rootPidValue)
  if (sort) {
    tree.sort(sort)
  }
  var stack = new Array(...tree)
  while (stack.length > 0) {
    var node = stack.pop()
    var subNodes = arr.filter(r => r[pidProp] === node[idProp])
    if (sort) {
      subNodes.sort(sort)
    }
    if (subNodes.length > 0) {
      stack.push(...subNodes)
      node[childrenProp] = subNodes
    }
  }
  return tree
}

export function cleanTree(nodes, callback) {
  let i = 0
  for (i = 0; i < nodes.length; i++) {
    var node = nodes[i]
    if (!node.children || node.children.length === 0) {
      if (callback instanceof Function) {
        if (callback(node) !== true) {
          continue
        }
      }
      // 删除
      nodes.splice(nodes.indexOf(node), 1)
      i--
    } else {
      cleanTree(node.children, callback)
      if (callback instanceof Function) {
        if (callback(node) !== true) {
          continue
        }
      }
      nodes.splice(nodes.indexOf(node), 1)
      i--
    }
  }
}

export function findIds(list, upArray, dataSource) {
  dataSource.forEach((item) => {
    const array = [...upArray, item.id]
    if (item.nodeType !== 0) {
      list.push(array)
    }
    if (item.children) {
      findIds(list, array, item.children)
    }
  })
  return list
}

export function moveUp(list, item, cb) {
  if (!item)
    return
  var idx = list.indexOf(item)
  if (idx > 0) {
    swap(list, idx, idx - 1)
    if (cb instanceof Function) {
      cb(list)
    }
  }
}

export function moveDown(list, item, cb) {
  if (!item)
    return
  var idx = list.indexOf(item)
  if (idx < list.length - 1) {
    swap(list, idx, idx + 1)
    if (cb instanceof Function) {
      cb(list)
    }
  }
}

export function moveTop(list, item, cb) {
  var idx = list.indexOf(item)
  list.splice(idx, 1)
  list.unshift(item)
  if (cb instanceof Function) {
    cb(list)
  }
}

export function moveBottom(list, item, cb) {
  var idx = list.indexOf(item)
  list.splice(idx, 1)
  list.push(item)
  if (cb instanceof Function) {
    cb(list)
  }
}

export function tree2array(tree, childrenProp, filter) {
  var result = []
  childrenProp = childrenProp || 'children'

  var subArrs = [tree]
  while (subArrs.length > 0) {
    arr = subArrs.pop()
    if (filter) {
      arr = arr.filter(filter)
    }
    for (const node of arr) {
      if (!node[childrenProp] || node[childrenProp].length === 0) {
        result.push(node)
      } else {
        subArrs.push(node[childrenProp])
      }
    }
  }
  return result
}

function swap(arr, from, to) {
  var tmp = arr.splice(from, 1)[0]
  arr.splice(to, 0, tmp)
}

