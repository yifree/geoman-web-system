import router from './router'
import store from './store'
import { ElMessage } from 'element-plus'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken, setToken } from '@/utils/auth' // get token from cookie

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/register'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()
  // set page title
  if (!store.state.app.info.appName) {
    await store.dispatch('app/getAppInfo')
  }
  document.title = store.state.app.info.appName.value
  if (whiteList.indexOf(to.path.toLowerCase()) >= 0) {
    next()
    return
  }
  // determine whether the user has logged in
  const hasToken = getToken()
  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      //await store.dispatch('user/resetToken')
      router.push(store.state.app.info.home_page.value)
      next()
      NProgress.done()
    } else {
      const hasGetUserInfo = store.state.user.info
      if (hasGetUserInfo) {
        next()
      } else {
        try {
          // get user info
          await store.dispatch('user/getInfo')
          await store.dispatch('app/getAppInfo')
          next()
        } catch (error) {
          console.log(error)
          // remove token and go to login page to re-login
          //await store.dispatch('user/resetToken')
          //ElMessage.error(error || 'Has Error')
          next(`/login?redirect=${encodeURIComponent(to.fullPath)}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
    if (to.query && to.query.query) {
      var q = JSON.parse(to.query.query)
      if (q['token'] && q['timestamp'] && q['userid']) {
        await store.dispatch('user/login2', q)
        router.push('/')
      }
      next()
    } else if (to.query && to.query['x-token']) {
      setToken(to.query['x-token'])
      await store.dispatch('user/setToken', to.query['x-token'])
      router.push('/')
      next()
    } else {
      if (whiteList.indexOf(to.path) !== -1) {
        // in the free login whitelist, go directly
        next()
      } else {
        // other pages that do not have permission to access are redirected to the login page.
        next(`/login?redirect=${encodeURIComponent(to.fullPath)}`)
        NProgress.done()
      }
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
