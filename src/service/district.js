import http from './request'
export default {
  list: () => http.post('/dict/list'),
  create: (body) => http.post('/dict/create', body),
  edit: (body) => http.post('/dict/edit', body),
  remove: (id) => http.post('/dict/delete', null, { params: { id } }),
  listItem: (dictId) => http.post('/dict/item/list', null, { params: { dictId } }),
  createItem: (body) => http.post('/dict/item/create', body),
  editItem: (body) => http.post('/dict/item/edit', body),
  removeItem: (id) => http.post('/dict/item/delete', null, { params: { id } }),
}