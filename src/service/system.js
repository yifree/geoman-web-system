import http from './request'
export default {
  list: () => http.post('/config/list'),
  get: (key) => http.post('/config/get', null, { params: { key } }),
  save: (body) => http.post('/config/save', body),
  remove: (id) => http.post('/config/delete', null, { params: { id } }),
}