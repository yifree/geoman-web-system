import http from './request'
export default {
  getInfo: () => http.get('/app/info')
}