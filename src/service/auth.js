import http from './request'
export default {
  login: (form) => http.post('/auth/login', form),
  logout: () => http.post('/auth/logout'),
  getInfo: () => http.get('/auth/info'),
  regist: (form) => http.post('/auth/regist', form),
}