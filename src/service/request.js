import axios from 'axios'
import router from '@/router'
import { ElMessage } from 'element-plus'

const http = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 10000
})

http.interceptors.request.use((config) => {
  config.withCredentials = true
  return config
}, async error => {
  console.log(error);
  throw error
})

http.interceptors.response.use(response => {
  switch (response.status) {
    case 200:
      if (response.headers['content-type']?.indexOf('json') < 0) {
        return res;
      }
      var resp = response.data
      switch (resp.code) {
        case 200:
          return resp
        case 401:
          if (route.currentRoute.path != '/login') {
            router.push('/login')
          }
          break
        case 500:
          break
        default:
          break
      }
      if (resp.msg) {
        ElMessage({ type: 'error', message: resp.msg })
        throw Error(resp.msg)
      } else {
        throw Error('请求发生错误')
      }
      break
    case 401:
      break
    case 404:
      break
    case 500:
      break
  }
}, async error => {
  ElMessage({ message: error, type: 'error', duration: 1000 * 2 })
  console.log(error);
  throw error
})

export default http;