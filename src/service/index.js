export { default as appApi } from './app'
export { default as userApi } from './user'
export { default as authApi } from './auth'
export { default as dictApi } from './dict'
export { default as districtApi } from './district'
export { default as funcApi } from './func'
export { default as groupApi } from './group'
export { default as logApi } from './log'
export { default as roleApi } from './role'
export { default as configApi } from './system'

// export default {
//   userApi,
//   authApi,
//   dictApi,
//   districtApi,
//   funcApi,
//   groupApi,
//   logApi,
//   roleApi
// }