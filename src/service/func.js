import http from './request'
export default {
  list: (query) => http.post('/function/list', null, { params: query }),
  create: (body) => http.post('/function/create', body),
  edit: (body) => http.post('/function/edit', body),
  remove: (id) => http.post('/function/delete', null, { params: { id } }),
}