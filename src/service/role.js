import http from './request'
export default {
  list: (params) => http.post('/role/list', null, { params }),
  listFunc: (roleId) => http.get('/role/func', { params: { roleId } }),
  create: (body) => http.post('/role/create', body),
  edit: (body) => http.post('/role/edit', body),
  remove: (id) => http.post('/role/delete', null, { params: { id } }),
  func: (roleId, body) => http.post('/role/func', body, { params: { roleId } })
}