import http from './request'

export default {
  list: (params) => http.post('/user/list', null, { params }),
  create: (body) => http.post('/user/create', body),
  edit: (body) => http.post('/user/edit', body),
  info: (userId) => http.post('/user/info', null, { params: { userId } }),
  changePsw:(body)=>http.post('/user/changePsw', body),
  remove: (userId) => http.post('/user/delete', null, { params: { userId } }),
  lock: (userId, lock) => http.post('/user/lock', null, { params: { userId, lock } }),
}