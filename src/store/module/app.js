import { appApi, dictApi } from '@/service'
const state = {
  dict: [],
  info: {},
  device: 'desktop'
}

export default {
  namespaced: true,
  state,
  mutations: {
    SET_APPINFO(state, info) {
      state.info = { ...info }
    },
    SET_DICT(state, dict) {
      state.dict = dict
    }

  },
  actions: {
    async getAppInfo({ commit, state }) {
      var data = await appApi.getInfo()
      commit('SET_APPINFO', data.value)
      return data
    },
    async getDict({ commit, state }) {
      var data = await dictApi.list()
      commit('SET_DICT', data.value)
      return data
    }
  }
}
