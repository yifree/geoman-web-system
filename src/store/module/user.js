import { getToken, setToken, removeToken } from '@/utils/auth'
import { authApi } from '@/service'
import { cleanTree } from '../../utils/array2tree'

const getDefaultState = () => {
  return {
    token: getToken(),
    info: null,
    funcs: [],
    menu: []
  }
}

const state = getDefaultState()


export default {
  namespaced: true,
  state,
  mutations: {
    SET_INFO: (state, info) => {
      state.info = info
    },
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_FUNCS: (state, funcs) => {
      state.funcs = funcs
    },
    RESET_STATE: (state) => {
      Object.assign(state, getDefaultState())
    },
    SET_MENU: (state, menu) => {
      state.menu = menu
    }
  },
  actions: {
    async login({ commit }, form) {
      var res = await authApi.login(form)
      var token = res.value
      commit('SET_TOKEN', token)
      setToken(token)
    },
    async logout({ commit, state }) {
      await authApi.logout()
      removeToken();
      commit('RESET_STATE')
    },
    async getInfo({ commit, state }) {
      var { value: data } = await authApi.getInfo()

      cleanTree(data.menu, n => {
        if (n.children && n.children.length > 0) {
          n.title = n.name
          return false
        } else {
          var found = data.funcs.find(r => r.id === n.func)
          if (found) {
            n.to = found.url
            n.title = n.name
          } else {
            return true;
          }
        }
      })
      console.log(data.menu);
      commit('SET_MENU', data.menu)
      commit('SET_INFO', data.user)
      commit('SET_FUNCS', data.funcs)
      return data
    }
  }
}
