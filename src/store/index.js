import { createStore } from 'vuex'
import user from './module/user'
import app from './module/app'

const store = createStore({
  modules: {
    user,
    app
  },
  getters: {}
})

export default store