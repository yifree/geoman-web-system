import { createApp, ssrContextKey } from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'
import './permission'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import './styles/index.scss'


const app = createApp(App)
app.config.productionTip = false

import * as ElementIcons from '@element-plus/icons-vue'
for (const key in ElementIcons) {
  const element = ElementIcons[key];
  app.component(key, element)
}


import * as CommonComponents from "@/components/common";
for (const key in CommonComponents) {
  const element = CommonComponents[key];
  app.component('Hjw' + key, element)
}

app
  .use(ElementPlus, { locale: zhCn, size: 'small' })
  .use(store)
  .use(router).mount('#app')

export default app